
# Nom del projecte
ITB Gym
# Temàtica
Pagina web per fer exercici fisic en temps de confinament
# Descripció de les pàgines
El nostre index sera una petita introducció del que tractarem a la web, el qual tindra un header fixe que ens permetra mourens entre les diferents seccions
- Pagina amb exercicis per a nivell basic
- Pagina amb exercicis per a nivell intermig
- Pagina amb exercicis per a nivell avançat

Em pensat fer 3 pagines diferents depenent dels recursos i nivell dels usuaris

## Integrants
- Àlex Rusiñol Gonzalez
- Pol Barragán Martínez